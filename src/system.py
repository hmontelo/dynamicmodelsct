# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  system.py
#
# Description:
#            This file implements the isystem interface and it is the base 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from object     import Object
from isystem    import ISystem

#
# Base class of all the linear/non-linear discrete time systems
#
class System(Object, ISystem):
       
    n_states = 0        # number of states
    n_inputs = 0        # number of inputs
    n_outputs= 0        # number of outputs
    Ts = 0              # Sample time interval
    x0 = None
    x  = None
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # System(self, n_states, n_inputs, n_outputs, Ts, x0)
    # Constructor method
    #
    # $Inputs:
    #    n_states : the length of the state column vector
    #    n_inputs : the number of system inputs, i.e., the length of the column 
    #    n_outputs: the number of system outputs, i.e., the length of the column  
    #    Ts : the sampling interval
    #    x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, n_states, n_inputs, n_outputs, Ts, x0 ):
        # number of states
        self.n_states = n_states
        
        # number of inputs
        self.n_inputs = n_inputs
        
        # number of outputs
        self.n_outputs = n_outputs
        
        # sampling time
        self.Ts = Ts
        
        # check initial condition
        self.x0 = np.asmatrix(x0)
        if not self.x0.shape == ( self.n_states, 1):
            raise ValueError("Wrong shape of initial state vector")
            
        self.x = np.matrix(x0)
    
    #
    # MeasureOutputs()
    # Get outputs, computed from system state
    # $Inputs:
    #    none
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    #     
    def MeasureOutputs( self ):
        pass
    
    #
    # ApplyInput()
    # Apply single input control and update system state
    # $Inputs:
    #    u: Additional input
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    # 
    def ApplyInput( self, u ):
        pass
    
    #
    # Simulate( u )
    # Simulate open-loop system dynamics and get back measurements of the outputs
    # $Inputs:
    #    u: It can be a a single input or a sequence of inputs
    # $Outputs:
    #    It returns the outputs of the system at each time step of the sct_simulation
    #
    def Simulate( self, u ):
        # set data to proper shape
        u = np.asmatrix(u)
        
        # initialize outputs array. Each column is the output at time k.
        y = np.matrix( np.zeros( (self.n_outputs, u.shape[1] ) ) )
        
        # for each time step 
        for i in range(u.shape[1]):
            # get measurements of the system
            y[:,i] = self.MeasureOutputs()
                    
            # compute new state vector
            self.ApplyInput( u[:,i] )
            
        return np.array( y )
    