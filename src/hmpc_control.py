# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  hmpc_control.py
#
# Description:
#            This file contains the HMPC (Hybrid Model Predictive Control) class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from control import Control

#
# Class that inherits the base Control standard definition and defines specific
# attributes and methods related to Hybrid Model predictive Control
#
class HmpcControl(Control):
    
    #
    # Init()
    # Initialize the Hmpc control.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def Init(self):
        pass
        
    #
    # PrintInit()
    # Print initial conditions of the Hmpc control.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def PrintInit(self):
        print "Initial Conditions for HMPC:"
        