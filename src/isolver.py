# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  isolver.py
#
# Description:
#            This file contains the abstract solver class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#


# Importing specific modules
import abc

#
# Interface with base methods used for all control classes that
# implements this interface. 
#
class ISolver(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def Solve(self):
        """Solve differential equations."""
        return
    
    def Success(self):
        """Return always true if no fail is found."""
        return True
    


    
