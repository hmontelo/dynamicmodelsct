# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  behavior.py
#
# Description:
#            This file contains the definition of 'a object' class.
#            The object class is the basis class used for all the framework.
#
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from iobject import IObject

#
# Base object class. This class defines all the fundamental methods and attributes
# into the isystem.
# It implements the interface IObject. 
#
class Object(IObject):
   
    # Object's value
    value = 0

    # 
    # __init__(name)
    # Constructor method.   
    # $Inputs:
    #    name: the object's name
    # $Outputs:
    #    none    
    #    
    def __init__(self, name):
        self.name = name    # instance variable unique to each instance,
                            # it represents the Object's name.
                            
    # 
    # Init()
    # Initialization method.   
    # $Inputs:
    #    none
    # $Outputs:
    #    none    
    #                              
    def Init(self):
        pass 
    
    # 
    # PrintInit()
    # Print the object's initial conditions.   
    # $Inputs:
    #    none
    # $Outputs:
    #    none    
    # 
    def PrintInit(self):
        print "Name: ", self.getName(), "\tValue: ", self.getValue()
                          
 
    # 
    # setName(name)
    # Set Object's Name    
    # $Inputs:
    #    name: Object's name.
    # $Outputs:
    #    none    
    #                     
    def setName(self, name):
        self.name = name  
   
    # 
    # getName(none)
    # Get Object's Name    
    # $Inputs:
    #    none    
    # $Outputs:
    #    Returns the object's name    
    #   
    def getName(self):
        return self.name 

    # 
    # setName(value)
    # Set Object's value    
    # $Inputs:
    #    name: Object's value.
    # $Outputs:
    #    none    
    #                       
    def setValue(self, value):
        self.value = value  
        
    # 
    # getValue(none)
    # Get Object's value    
    # $Inputs:
    #    none    
    # $Outputs:
    #    Returns the object's value    
    #    
    def getValue(self):
        return self.value
    