# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  sct_simulation.py
#
# Description:
#            This file contains class definition for the Simulation Environment
#            specialized for use MPC (Model Predictive Control) on a vehicle
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from math import *

from simulation         import Simulation
from system             import System
from control            import Control
from observer           import Observer 
from simulation_results import SimulationResults

#
# Class definition for the Simulation Environment
# specialized for use MPC (Model Predictive Control) on a vehicle.
#   
class SimulationEnvironment(Simulation):
    
    system     = None #System()
    controller = None #Control()
    controller = None #Observer()

    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # SimulationEnvironment(system, controller=None, observer=None)
    # Constructor method
    #
    # $Inputs:
    #   system : This is the system we want to simulate.       
    #   controller : The feedback controller.       
    #   observer : A state observer
    # $Outputs:
    #    none
    #
    def __init__ ( self, system, controller=None, observer=None ):
        # the system we want to simulate
        self.system = system
        
        # the controller ( state feedback )
        self.controller = controller

        # the state observer
        self.observer = observer
        
    #
    # Run(Tsim, printInternalResults = False)
    # Starts and simulate controlled system dynamics.
    # $Inputs
    #    Tsim : the length of the simulation, in seconds.
    #    printInternalResults: Print internal results? (if yes
    #                          starts to print calculated values by the solver.
    #                          Useful for debug.
    # $Outputs:
    #    Returns the simulation results
    #        
    def Run( self, Tsim, printInternalResults = False):
        
        # update simulation time
        self.setSimulationTime(Tsim)
        
        # run for sufficient steps
        n_steps = int( Tsim / self.system.Ts ) + 1
        
        # Preallocate matrices
        # This matrix is for the state 
        xhat = np.zeros( (self.system.n_states, n_steps) )
        
        # control input
        u = np.zeros( (self.system.n_inputs, n_steps) )
        
        # measurements
        y = np.zeros( (self.system.n_outputs, n_steps) )
        
        
        if self.observer:
            # if we have an observer estimate system's state
            xhat[:,0] = self.observer.getEstimatedStates( 
                self.system.MeasureOutputs().ravel(), u[:,0]  ).ravel()
        else:
            # try to compute measurements, but only if the system is observable.
            try:
                xhat[:,0] = (np.linalg.inv(self.system.C) * self.system.MeasureOutputs()).ravel()
            except np.linalg.LinAlgError:
                raise ValueError("System is not observable. Cannot compute system's state.")
                
        
        # run simulation
        for k in xrange( n_steps-1 ):
            
            if self.controller:           
                # compute control move based on the state at this time. 
                u[:,k] = self.controller.ComputeControlInput( xhat[:,k].reshape(self.system.n_states,1) )
            else:
                #### TODO ###
                raise ValueError("None controller")
            
            # apply input 
            self.system.ApplyInput( u[:,k].reshape(self.system.n_inputs, 1) )
            
            # get measurements
            y[:,k+1] = self.system.MeasureOutputs().ravel()
            
            # now we are at step step k+1
            # estimate state using observer based on output at current 
            # step and previous control input value
            if self.observer:
                xhat[:,k+1] = self.observer.getEstimatedStates( y[:,k+1], u[:,k] ).ravel()
            else:
                xhat[:,k+1] = (np.linalg.inv(self.system.C) * self.system.MeasureOutputs() ).ravel()
                
        return SimulationResults(xhat, u, y, self.system.Ts)   
    
    
    