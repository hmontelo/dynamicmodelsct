# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main_single_integrator.py
#
# Description:
#            This file contains the main function used to start the software.
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system
import numpy as np

from pylab import *
from control import Control
from single_integrator import SingleIntegrator
from non_linear_control import NonLinearController
from simulation_environment import SimulationEnvironment
       
               
if __name__ == '__main__':
    
    # define system
    single_integ = SingleIntegrator( Ts=10e-3, w0=1e-4, x0=np.matrix([[5]]) )
    
    # define non-linear controller subject to saturation
    nl_controller = NonLinearController( K = 50, alpha=0.9, saturation=(-4, 0.5) )
    
    # create simulator
    sim = SimulationEnvironment( single_integ, controller=nl_controller )

    # simulate system for one second
    res = sim.Run( 5.0 )

    # do plots

    
    subplot(211)
    plot ( res.t, res.x[0] )
    ylabel('e [m]')
    grid()
    
    subplot(212, sharex=gca())
    plot ( res.t, res.u[0] )
    ylabel('u')
    xlabel('t [s]')
    grid()
    
    
    show()