# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main_dynamics.py
#
# Description:
#            This file contains the main function used to simulate a software
#            that simulates the dynamics of a vehicle that is accelerated at
#            constant rate on a straight. It is measured the vehicle's position,
#            but considering an embedded GPS system with low accuracy and not trustful.
#            A Kalman filter is used to estimate the vehicle's position.
#
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system
import numpy as np
from pylab import *

from vehicle import Vehicle                              # It contains the definition of the Vehicle class
from throttle import Throttle                            # It contains the definition of the Throttle class
from kalman_state_observer import KalmanStateObserver    # It contains the definition of the Kalman State Observer class
from simulation_environment import SimulationEnvironment # It contains the definition of the Vehicle Simulation class
from sys            import exit                          # Importing the exit() function from the Python's system library.

# Main function call
if __name__ == '__main__':

    # sampling time. 
    Ts = 0.05
    
    # Process and measurement noise covariance matrices
    # this value should be the variance of the measurement position error.
    # Our GPS is quite crappy so i have set a Root Mean Square Error of 
    # 10 meters
    Sv = np.matrix( [[10.0**2]] )
    
    # This is a little bit less intuitive to get.
    # It accounts for the fact that even if we press the pedal to full,
    # we do not get the expected accelleration, because of the fact the 
    # our car is running in an environment with gusts, holes on the road, 
    # ..., which introduce a random variation in the car's accelleration.
    Sw = 1e1 * np.matrix( [[0.25*Ts**4, 0.5*Ts**3],[0.5*Ts**3, Ts**2]] )
       
    # Ok, now define the system, giving zero initial conditions.
    vehicle = Vehicle( Ts=Ts, Sw=Sw, Sv=Sv, x0 = np.matrix( [[0.0],[0.0]] ) )
    
    # define controller. We want a constant value of input. Remember to 
    # put a value for the acceleration in m/s^2, and not g!
    throttle = Throttle( value=9.81*1 ) 
    
    # create kalman state observer object
    kalman_observer = KalmanStateObserver( vehicle )
    
    # Create simulator.
    # This is our simulation object; it is responsible of making all 
    # the parts communicating together.
    sim = SimulationEnvironment( vehicle, throttle, kalman_observer )
        
    # run simulation for 10 seconds
    res = sim.Run( 10 )
        
    # now do plots   
    subplot(311)
    plot ( res.t, res.y[0], 'b.', label='Position measured' )
    plot ( res.t, res.x[0], 'r-', lw=2, label='Position estimated' )
    legend(loc=2, numpoints=1)
    ylabel( 'x [m]' )
    grid()
    
    subplot(312, sharex=gca())
    plot ( res.t, res.x[1], 'r-', label='Velocity estimated' )
    ylabel( 'v [m/s]' )
    legend(loc=2, numpoints=1)
    grid()
    
    subplot(313, sharex=gca())
    plot ( res.t, res.u[0], 'b-', label='Input' )
    xlabel( 't [s]' )
    ylabel( 'u' )
    legend(loc=2, numpoints=1)
    grid()
    
    show()
    
    # Finish
    exit(0)

