# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  sct_model.py
#
# Description:
#            This file contains the SCT(Social Cognition Theory) class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy            as np
from model              import Model
from behavior           import Behavior
from cue                import Cue
from expectancy         import Expectancy
from scipy.integrate    import odeint



# The SctModel class represents the model described in the following article:
# Martin, C. et al. "A Dynamical System Model of Social Cognitive Theory".
#   June 4-6, 2014. American Control Conference (ACC). 2014.
#
class SctModel(Model):
    
    ### Model's constants ###
    
    # Max number of inflow resistances
    MAX_INFLOW_RESISTANCE = 69 # Note: Index = 0 is not used (using indexes  from 1 to 68)
    
    # Max number of outflow resistances
    MAX_OUTFLOW_RESISTANCE = 55 # Note: Index = 0 is not used (using indexes  from 21 to 54)
    
    # Max number of exogenous inputs
    MAX_EXO_INPUTS = 9  # Note: It is included 8 + 1 slots in the inventory, just to start the index count from 1.
                        # Index = 0 is not used (using indexes  from 1 to 8)
    
    # Max number of inventories/Statuses 
    MAX_INVENTORIES = 7 # Note: It is included 6 + 1 slots in the inventory, just to start the index count from 1.
                        # Index = 0 is not used (using indexes  from 1 to 6)
    
    # Max number of indexes used for the time constant array
    MAX_TIME_CONST = 7 # Index = 0 is not used (using indexes  from 1 to 6) 
     
    # Max number of indexes used for the time delay array
    MAX_TIME_DELAY = 22 # Index = 0 is not used (using indexes  from 1 to 21)    
    
    # Max number of indexes used for the disturbance array
    MAX_DISTURBANCE = 7 # Index = 0 is not used (using indexes  from 1 to 6)  
    
    # Max external cues array size
    MAX_EXTERNAL_CUE_VALUES = 200     
    
    # Max Barriers array size
    MAX_BARRIERS_VALUES = 200   
    
    # Max Social Support array size 
    MAX_SOCIAL_SUPPORT_VALUES = 200
    
    # Max Observed Behavior array size
    MAX_OBSERVED_BEHAVIOR_VALUES = 200         
    
    ### Inventories/Statuses indexes (make easy to search and/or update the respective statuses) ###
    
    SKILLS           = 1  # Skills training. These activities help to increase (or decrease) self-management skills.
    OUTCOME_EXPECTED = 2  # The representative volume of the expected individual outcome.
    CONFIDENCE       = 3  # It contains the volume associated with the individual confidence.
    BEHAVIOR         = 4  # It represents the internal individual behavior.
    BEHAVIOR_OUTCOME = 5  # It represents the volume of the individual outcome behavior.
    WILL_POWER       = 6  # It consists of the individual will power.
      
      
    ### Model's array's ###
    
    # inflow resistances array
    gamma = np.zeros((MAX_INFLOW_RESISTANCE,1)) #[0] * MAX_INFLOW_RESISTANCE 
    
    # outflow resistances array
    beta = np.zeros((MAX_OUTFLOW_RESISTANCE,1)) # [0] * MAX_OUTFLOW_RESISTANCE
    
    # Exogenous inputs
    xi = np.zeros((MAX_EXO_INPUTS,1)) # [0] * MAX_EXO_INPUTS
    
    # Inventories/Statuses (or current conditions)
    eta = np.zeros((MAX_INVENTORIES,1)) # [0] * MAX_INVENTORIES
    
    # Time constant array
    tau = np.ones((MAX_TIME_CONST,1)) # [1] * MAX_TIME_CONST
    
    # Time delay array
    theta = np.zeros((MAX_TIME_DELAY,1)) # [0] * MAX_TIME_DELAY
    
    # Disturbance array
    sigma = np.zeros((MAX_DISTURBANCE,1)) # [0] * MAX_DISTURBANCE
    
    # Array that store the External Cues values
    ExternalCue = np.zeros((MAX_EXTERNAL_CUE_VALUES,1))
    
    # Array that store the Observed Behavior values
    ObservedBehavior = np.zeros((MAX_OBSERVED_BEHAVIOR_VALUES,1))
    
    # Array that store the Social Support values
    SocialSupport = np.zeros((MAX_SOCIAL_SUPPORT_VALUES,1))
    
    # Array that store the Barriers values
    Barriers = np.zeros((MAX_BARRIERS_VALUES,1))
    
    
    ### Model's constants ###
    
    # list of behaviors
    b1 = Behavior("Behavior1")
    b2 = Behavior("Behavior2")
    
    # list of behaviors
    c1 = Cue("C1")
    c2 = Cue("C2")
    
    #
    # Scenario1()
    # This method represents the first scenario used to obtain the sct_simulation results.
    # Different scenarios can be created by adjusting the initial conditions 
    # and/or using the graphical user interface to input data.
    # $Inputs
    #    none
    # $Outputs:
    #    none
    #
    def Scenario1(self):
        
        # Initializing inventories array
        self.eta[self.SKILLS]           = 0
        self.eta[self.OUTCOME_EXPECTED] = 0
        self.eta[self.CONFIDENCE]       = 0
        self.eta[self.BEHAVIOR]         = 0
        self.eta[self.BEHAVIOR_OUTCOME] = 0
        self.eta[self.WILL_POWER]       = 0
        
        # Initializing Time constant array
        self.tau[1] = 1 #
        self.tau[2] = 1 #
        self.tau[3] = 1 #
        self.tau[4] = 2 #
        self.tau[5] = 1 #
        self.tau[6] = 3 #
        
        # Initializing Inflow Resistance arrays
        self.gamma[11] = 0.8  #
        self.gamma[22] = 0.75 #
        self.gamma[32] = 1.6  #
        self.gamma[33] = 0.75 #
        self.gamma[35] = 1    #
        self.gamma[36] = 1    #
        self.gamma[57] = 2    #
        self.gamma[64] = 20   #
        self.gamma[68] = 5    #
        
        # Initializing outflow Resistance arrays
        self.beta[21] = 0.3    #
        self.beta[31] = 0.5    #
        self.beta[42] = 0.3    #
        self.beta[43] = 0.8    #
        self.beta[45] = 0.0    # 
        self.beta[54] = 0.3    #
        self.beta[34] = 0.2    #
        self.beta[25] = 0.3    #
        self.beta[14] = 0.23   #
        self.beta[46] = 0.435  #
        
        #self.xi[2] = 3
        #self.xi[3] = 3
        #self.xi[5] = 10
        
        
        self.b1.setValue(0.001)
        self.b2.setValue(0.003)
        
    
    #
    # Init(num_steps, i)
    # Initialization method
    # Used to load different scenarios and respective initial conditions.
    # $Inputs:
    #    num_steps: The calculated number of steps to be used in the sct_simulation process.
    #               This value is used to initialize internal arrays.
    #    i:         A scenario selector (0 means the scenario #1) 
    # $Outputs:
    #    none
    #
    def Init(self, num_steps, i=0):
        
        self.ObservedBehavior   = np.zeros((num_steps,1))
        self.ExternalCue        = np.zeros((num_steps,1))
        if i == 0:
            self.Scenario1()
        
    #
    # PrintInit()
    # Optional: Print some initial values... useful for debug.
    # $Inputs:
    #    none
    # $Outputs:
    #    none    
    #
    def PrintInit(self):
        print "Initial Conditions for SCT Model:"
        print "Name: ", self.b1.getName(), "\tValue: ", self.b1.getValue()
        print "Name: ", self.b2.getName(), "\tValue: ", self.b2.getValue()
        
        # printing inflow resistances
        for idx, i in enumerate(self.gamma):
            if (idx > 0) and (idx < self.MAX_INFLOW_RESISTANCE):
                print "gamma[",idx,"]=", i
    
    #
    # Eval_Behavior(confidence, willpower, outcome_expectance, behavioral_outcome, disturbance )
    # Method that used to handle specifically the 'Behavior' container.  
    # $Inputs:
    #    confidence:             confidence level
    #    willpower:              will power level
    #    outcome_expectance:     outcome expected level
    #    behavioral_outcome:     behavioral outcome level
    #    disturbance:            disturbance level
    # $Outputs:
    #    none    
    #
    def Eval_Behavior(self, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance ):
        pass
    
    #
    # Eval_Efficacy(behavior)
    # Method that used to handle specifically the 'Efficacy' container.  
    # $Inputs:
    #    behavior:               behavior level
    # $Outputs:
    #    none    
    #
    def Eval_Efficacy(self, behavior):
        pass

    #
    # Eval_Behavioral_Outcome(behavior, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance)
    # Method that used to handle specifically the 'Behavior Outcome' container.  
    # $Inputs:
    #    behavior:               behavior level    
    #    confidence:             confidence level
    #    willpower:              will power level
    #    outcome_expectance:     outcome expected level
    #    behavioral_outcome:     behavioral outcome level
    #    disturbance:            disturbance level
    # $Outputs:
    #    none    
    #    
    def Eval_Behavioral_Outcome(self, behavior, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance):
        self.Eval_Behavior(confidence, willpower, outcome_expectance, behavioral_outcome, disturbance)
        self.Eval_Efficacy(behavior)        

    # Evaluate(behavior, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance)
    # General Method that evaluate all other containers   
    # $Inputs:
    #    behavior:               behavior level    
    #    confidence:             confidence level
    #    willpower:              will power level
    #    outcome_expectance:     outcome expected level
    #    behavioral_outcome:     behavioral outcome level
    #    disturbance:            disturbance level
    # $Outputs:
    #    none    
    #    
    def Evaluate(self, behavior, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance):
        self.Eval_Behavioral_Outcome(behavior, confidence, willpower, outcome_expectance, behavioral_outcome, disturbance)
        return
    
    #
    # model(t, y)
    # Method that contains the dynamic equations of the isystem.
    # This method calculates the following derivatives:
    # - Skills training
    # - Outcome Expected
    # - Confidence level
    # - Behavior
    # - Behavior Outcome 
    # - and Will Power
    # The method returns an array with the respective derivative in the same sequence.
    # $Inputs:
    #    t : time
    #    y : an array with the current values for Skills, Outcome Expected, Confidence level,  
    #        Behavior, Behavior Outcome, ad Will Power.
    # $Outputs: 
    #    dydt: array with the respective derivatives.
    #
    def model(self, t, y):
        
        n = len(y)      # implies we have n ODEs
        dydt = np.zeros((n,1))
        
        self.eta[self.SKILLS]            = y[0]
        self.eta[self.OUTCOME_EXPECTED]  = y[1]
        self.eta[self.CONFIDENCE]        = y[2]
        self.eta[self.BEHAVIOR]          = y[3]
        self.eta[self.BEHAVIOR_OUTCOME]  = y[4]
        self.eta[self.WILL_POWER]        = y[5] 
        
        self.xi[8] = 0
        if (t > 1) and (t < 14):
            self.xi[8] = 6
        
        #self.xi[2] = 0        
        #if (t > 5) and (t < 14):
        #    print "here"
        #    self.xi[2] = 50
               
        # Differential equation for Skills training
        dydt[0]            = ( self.gamma[11]* self.xi[1]*(t-self.theta[1]) + \
                                         self.beta[14]*self.eta[self.BEHAVIOR]*(t-self.theta[16]) - \
                                         self.eta[self.SKILLS]*t + self.sigma[1]*t \
                                        )/self.tau[1]
                                        
        # Differential equation for Outcome Expected                                        
        dydt[1]             = ( self.gamma[22]* self.xi[2]*(t-self.theta[4]) + \
                                         self.beta[21]*self.eta[self.SKILLS]*(t-self.theta[2]) + \
                                         self.beta[25]*self.eta[self.BEHAVIOR_OUTCOME]*(t-self.theta[14]) - \
                                         self.eta[self.OUTCOME_EXPECTED]*t + self.sigma[2]*t \
                                       )/self.tau[2]
        
        # Differential equation for Confidence level                                
        dydt[2]             = ( self.gamma[32]* self.xi[2]*(t-self.theta[5]) + \
                                         self.gamma[33]* self.xi[3]*(t-self.theta[7]) - \
                                         self.gamma[35]* self.xi[5]*(t-self.theta[9]) + \
                                         self.gamma[36]* self.xi[6]*(t-self.theta[10]) + \
                                         self.beta[31]*self.eta[self.SKILLS]*(t-self.theta[3]) + \
                                         self.beta[34]*self.eta[self.BEHAVIOR]*(t-self.theta[13]) - \
                                         self.eta[self.CONFIDENCE]*t + self.sigma[3]*t \
                                      )/self.tau[3]
        
        # Differential equation for Behavior                               
        dydt[3]             = ( self.beta[42]*self.eta[self.OUTCOME_EXPECTED]*(t-self.theta[6]) + \
                                         self.beta[43]*self.eta[self.CONFIDENCE]*(t-self.theta[8]) + \
                                         self.beta[46]*self.eta[self.WILL_POWER]*(t-self.theta[17]) + \
                                         self.beta[45]*self.eta[self.BEHAVIOR_OUTCOME]*(t-self.theta[21]) - \
                                         self.eta[self.BEHAVIOR]*t + self.sigma[4]*t \
                                       )/self.tau[4]
        
        # Differential equation for Behavior Outcome
        dydt[4]             = ( self.gamma[57]* self.xi[7]*(t-self.theta[15]) + \
                                         self.beta[54]*self.eta[self.BEHAVIOR]*(t-self.theta[12]) - \
                                         self.eta[self.BEHAVIOR_OUTCOME]*t + self.sigma[5]*t \
                                       )/self.tau[5]
        
        # Differential equation for Will Power
        dydt[5]             = ( self.gamma[64]* self.xi[4]*(t-self.theta[11]) + \
                                         self.gamma[68]* self.xi[8]*(t-self.theta[18]) - \
                                         self.eta[self.WILL_POWER]*t + self.sigma[6]*t \
                                       )/self.tau[5]
        
        # Return value. An array with respective derivatives for:
        # - Skills training
        # - Outcome Expected
        # - Confidence level
        # - Behavior
        # - Behavior Outcome 
        # - and Will Power
        return dydt
    

        
    
    