# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  isystem.py
#
# Description:
#            This file contains the abstract system class with mandatory interface
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#


# Importing specific modules
import abc

#
# Interface with base methods used for all control classes that
# implements this interface. 
#
class ISystem(object):
    __metaclass__ = abc.ABCMeta
    
    #
    # MeasureOutputs()
    # Get outputs, computed from system state
    # $Inputs:
    #    none
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    #
    @abc.abstractmethod
    def MeasureOutputs( self ):
        """Get outputs, computed from system state.This is an interface: use derived classes."""
        raise NotImplemented( 'Call derived classes instead.')
    
    #
    # ApplyInput()
    # Apply single input control and update system state
    # $Inputs:
    #    u: Additional input
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    # 
    @abc.abstractmethod
    def ApplyInput( self, u ):
        """Apply single input control and update system state.This is an interface: use derived classes."""
        raise NotImplemented( 'Call derived classes instead.')

 


    
