# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  system.py
#
# Description:
#            This file defines a non-linear system class. 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
import scipy
import scipy.linalg
from system import System

#
# A class for linear time-invariant discrete time systems
#
class LTISystem(System):
    
   
    A = None #np.empty()     # the transition matrix.
    B = None #np.empty()     # the input matrix
    C = None #np.empty()     # the output matrix
    D = None #np.empty()     # output matrix
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # LTISystem(A, B, C, D, Ts, x0)
    # Constructor method
    #
    # $Inputs:
    #   A : the transition matrix. Must be square. dim = n_states x n_states
    #   B : the input matrix. Must have dim = n_states x n_inputs
    #   C : the output matrix. Must have dim = n_outputs x n_states           
    #   D : output matrix. Must have dim = n_outputs x n_inputs 
    #   Ts : the sampling interval
    #   x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, A, B, C, D, Ts, x0 ):
        
        # set state-space matrices
        self.A = np.matrix(A, copy=True)
        self.B = np.matrix(B, copy=True)
        self.C = np.matrix(C, copy=True)
        self.D = np.matrix(D, copy=True)
        
        # Consistency check
        if not self.A.shape[0] == self.A.shape[1]:
            raise ValueError("matrix A must be square")
        
        if not self.B.shape[0] == self.A.shape[0]:
            raise ValueError("matrix B must be have the same number of rows as matrix A")
            
        if not self.C.shape[1] == self.A.shape[0]:
            raise ValueError("matrix C must be have the same number of columns as matrix A")
            
        if not self.D.shape[0] == self.C.shape[0]:
            raise ValueError("matrix D must be have the same number of rows as matrix C'")
        
        System.__init__(self, self.A.shape[0], self.B.shape[1], self.C.shape[0], Ts, x0 )
        
    
    #
    # MeasureOutputs()
    # Get outputs, computed from system state
    # $Inputs:
    #    none
    # $Outputs:
    #    Generated outputs
    #     
    def MeasureOutputs( self ):
        return self.C * self.x
    
    #
    # ApplyInput()
    # Apply single input control and update system state
    # $Inputs:
    #    u: Additional input
    # $Outputs:
    #    Applied inputs
    # 
    def ApplyInput( self, u ):
        self.x = self.A * self.x + self.B * u

#
# PowerSeries(A, Ts, n)
# Convert continuous-time model to discrete time, 
# using a zero-order hold method
# $Inputs:
#    A:  Input Matrix
#    Ts: Sampling time
#    n:  Matrix's range
# $Outputs:
#    Returns the matrix power series.
# 
def PowerSeries(A, Ts, n=10):
    S = np.eye(A.shape[0])*Ts
    Am = np.matrix(A)
    for i in xrange(1,n):
        S +=  (Am**i) * Ts**(i+1) / scipy.misc.factorial(i+1)
    return S

#
# ContinuousToDiscrete( system, Ts )
# Convert continuous-time model to discrete time, 
# using a zero-order hold method
# $Inputs:
#    system: continuous model
#    Ts:     Discrete time
# $Outputs:
#    Returns a LTISystem.
# 
def ContinuousToDiscrete( system, Ts ):
    Ad = scipy.linalg.expm(system.A*Ts , q=7)
    Bd = PowerSeries( system.A, Ts, 10 )*system.B
    Cd = system.C
    Dd = system.D
    
    return LTISystem( Ad, Bd, Cd, Dd, Ts )    
                 
        
    