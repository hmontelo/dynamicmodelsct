# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  simulation_results.py
#
# Description:
#            This file contains class definition for the Simulation Results
#            A specialized class used to hold and show the simulation results
#            in a previous simulation execution.
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np


#
# A specialized class used to hold and show the simulation results
# in a previous simulation execution
#   
class SimulationResults( object ):
    
    x = None #np.empty()  # The time evolution of the system state
    u = None #np.empty()  # The time evolution of the control input 
    y = None #np.empty()  # The time evolution of the system's outputs
    t = None #np.empty()  # Time sampling

    #
    # SimulationResults(x, u, y, Ts)
    # Constructor method
    #
    # $Inputs:
    #   x : The time evolution of the system state. 
    #   u : The time evolution of the control input
    #   y : The time evolution of the system's outputs.
    #   Ts: Time sampling
    # $Outputs:
    #    none
    #
    def __init__ ( self, x, u, y, Ts ):
        self.x = x
        self.u = u
        self.y = y
        self.t = np.arange(x.shape[1]) * Ts  
    
    
    