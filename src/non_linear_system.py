# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  system.py
#
# Description:
#            This file defines a non-linear system class. 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from system import System

#
# A class for non-linear time-invariant discrete-time systems
#
class NonLinearSystem(System):
    
    f = None
    g = None
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        self.f = None
        self.g = None
    
    #
    # NonLinearSystem(self, f, g, n_states, n_inputs, n_outputs, Ts, x0)
    # Constructor method
    #
    # $Inputs:
    #    f : any 'python' function that accepts two arguments and must return 
    #        one argument
    #    g : any 'python' function that accepts two arguments and must return 
    #        one argument
    #    n_inputs: the number of inputs
    #    n_outputs: the number of system outputs, i.e., the length of the column  
    #    Ts : the sampling interval
    #    x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, f, g, n_states, n_inputs, n_outputs, Ts, x0):

        # state equation function
        self.f = f
        
        # outputs equation function
        self.g = g
        
        System.__init__(self, n_states, n_inputs, n_outputs, Ts, x0)
        
        
    