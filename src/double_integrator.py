# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  double_integrator.py
#
# Description:
#            This file defines a non-linear system class. 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from lti_system import LTISystem

#
# A class for linear time-invariant discrete time systems
#
class DoubleIntegrator(LTISystem):
       
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # DoubleIntegrator(Ts, x0)
    # Constructor method
    #
    # $Inputs:
    #   Ts : the sampling interval
    #   x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, Ts, x0 ):
        
        # state space matrices of the double integrator
        A = [[1, Ts], [0, 1]]
        B = [[Ts**2/2], [Ts]]
            
        # the state is fully observable
        C = [[1, 0], [0, 1]]
        D = [[0], [0]]
        
        LTISystem.__init__(self, A, B, C, D, Ts, x0 )
        
    