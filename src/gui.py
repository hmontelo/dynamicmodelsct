# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  gui.py
#
# Description:
#            Graphical User Interface Class definition (GUI)
#
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
import kivy
kivy.require('1.0.6')       # replace with your current kivy version !

from kivy.app               import App
from kivy.uix.gridlayout    import GridLayout
from kivy.uix.label         import Label
from kivy.uix.textinput     import TextInput
from kivy.uix.button        import Button
from kivy.logger            import Logger
from kivy.config            import Config

# 
# Class that represents the Graphical User Interface (GUI)
# This class is used to provide initial values used
# into the SCT model.
# The input values are saved into a text file and later they can be loaded 
# by other application. 
#
class GUI(GridLayout):
    
    stimuli_config_filename = 'sct_stimuli_config' # Text file
    stimuli_config          = []                   # Inout array
    
    # 
    # __init__()
    # initialization function
    # Method used to load the GUI's layout
    # and initial values.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def __init__(self, **kwargs):
               
        super(GUI, self).__init__(**kwargs)
        self.cols = 4
        
        # Training input
        self.add_widget(Label(text='Training (xi1):'))
        self.training = TextInput(multiline=False)
        self.add_widget(self.training)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day1 = TextInput(multiline=False)
        self.add_widget(self.day1)
        
        # Observed Behavior input
        self.add_widget(Label(text='Observed Behavior (xi2):'))
        self.obs_behavior = TextInput(multiline=False)
        self.add_widget(self.obs_behavior)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day2 = TextInput(multiline=False)
        self.add_widget(self.day2)
        
        # Support input
        self.add_widget(Label(text='Support (xi3):'))
        self.support = TextInput(multiline=False)
        self.add_widget(self.support)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day3 = TextInput(multiline=False)
        self.add_widget(self.day3)
        
        # Internal Cues input
        self.add_widget(Label(text='InternalCues (xi4):'))
        self.internal_cues = TextInput(multiline=False)
        self.add_widget(self.internal_cues)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day4 = TextInput(multiline=False)
        self.add_widget(self.day4)
        
        # Barriers input
        self.add_widget(Label(text='Barriers: (xi5'))
        self.barriers = TextInput(multiline=False)
        self.add_widget(self.barriers)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day5 = TextInput(multiline=False)
        self.add_widget(self.day5)
        
        # Mood input
        self.add_widget(Label(text='Mood: (xi6)'))
        self.mood = TextInput(multiline=False)
        self.add_widget(self.mood)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day6 = TextInput(multiline=False)
        self.add_widget(self.day6)
        
        # Environment input
        self.add_widget(Label(text='Environment: (xi7)'))
        self.env = TextInput(multiline=False)
        self.add_widget(self.env)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day7 = TextInput(multiline=False)
        self.add_widget(self.day7)
        
        # External cues input
        self.add_widget(Label(text='External Cues: (xi8)'))
        self.ext_cues = TextInput(multiline=False)
        self.add_widget(self.ext_cues)
        
        # Days
        self.add_widget(Label(text='Days(start-end):'))
        self.day8 = TextInput(multiline=False)
        self.add_widget(self.day8)
        
        # including "Ok" button
        buttonOk = Button(text='OK', font_size=14)
        
        # Callback function associated to the On-Click event on the button OK.
        # When the button OK is clicked, all values into the edit boxes are saved 
        # into the 'stimuli_config_filename' 
        #
        def callbackBtnOk(instance):
            print('Saving Stimuli Config' + self.stimuli_config_filename)
            self.stimuli_config.append(self.training.text)
            self.stimuli_config.append(self.obs_behavior.text)
            self.stimuli_config.append(self.support.text)
            self.stimuli_config.append(self.internal_cues.text)
            self.stimuli_config.append(self.barriers.text)
            self.stimuli_config.append(self.mood.text)
            self.stimuli_config.append(self.env.text)
            self.stimuli_config.append(self.ext_cues.text)
            self.save()
        
        # It associates the button OK and its callback.
        buttonOk.bind(on_press=callbackBtnOk)
        self.add_widget(buttonOk)

        # including "Cancel" button
        buttonCancel = Button(text='Cancel', font_size=14)
        
        # Callback function associated to the On-Click event on the button Cancel.
        def callbackBtnCancel(instance):
            print('The button <%s> is being pressed' % instance.text)
            App.get_running_app().stop()
        
        # It associates the button Cancel and its callback.
        buttonCancel.bind(on_press=callbackBtnCancel)
        self.add_widget(buttonCancel) 
    
    #
    # save()
    # Method used to save the input configuration file
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def save(self):
        fob = open(self.stimuli_config_filename,'w')
        for i in self.stimuli_config:
            fob.write(i + "\n")
        fob.close()         

# 
# Class that represents the application.
# Load the Graphical User Interface, and provided log
# for start and finish events.
#
class MyApp(App):
    
    def build(self):
        self.icon  = 'light_bulb_on_32x32.png'
        self.title = 'Social Cognitive Theory Stimulus Test Application'
        return GUI()
    
    def on_start(self):
        Logger.info('App: Starting application...')
    
    def on_stop(self):
        Logger.critical('App: Closing application...')


# main function of the isystem
if __name__ == '__main__':
    # configure the Windows size 
    Config.set('graphics', 'width', '450')
    Config.set('graphics', 'height', '260')
    
    # Execute Application
    MyApp().run()
    # Other way to change the window's size - after window's creation
    # Window.size = (300, 100)
    
    
    