# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  lq_control.py
#
# Description:
#            This file contains the implementation of a linear quadratic 
#            controller
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from   control      import Control
from riccati_solver import RiccatiSolver

#
# Class that inherits the base Control standard definition and defines specific
# attributes and methods related to Hybrid Model predictive Control
#
class LQControl(Control):
    
    K       = None #np.empty()        # create static state feedback matrix
    solver  = None #RiccatiSolver()   # Solver: Riccati (others could be selected:Lyapunov, OdeInt, etc...)
    
    ### Solver's integration methods ###
    Iterative_Method, Direct_Method, Cyclic_Method = range(3) 
    
    #
    # LQControl(system, Q, R )
    # Constructor method
    #
    # $Inputs:
    #    system : It represents the linear system which has 
    #            to be controlled.          
    #    Q : The state weight matrix. Must be positive definite
    #        and square.
      
    #    R : The input weight matrix. Must be positive definite.
    #        and square   
    # $Outputs:
    #    It updates the state feedback gain matrix
    #
    def __init__ ( self, system, Q, R, solver_method = Direct_Method, use_cyclic = False):
        # make two local variables
        Ql = np.matrix( Q )
        Rl = np.matrix( R )
        
        # Solve Algebraic Equation
        self.solver  = RiccatiSolver(system.A, system.B, Ql, Rl, solver_method, use_cyclic)
        P = self.solver.Solve()
        
        self.K  = (Rl + system.B.T * P * system.B).I * (system.B.T * P * system.A)
    
    #
    # ComputeControlInput( x  )
    # Compute static state feedback
    #
    # $Inputs:
    #    x : It is the the state column vector  
    # $Outputs:
    #    It returns an input vector which is feedback back to the isystem
    #
    def ComputeControlInput( self, x ):
        return -np.dot( self.K, x)     # u = - K x
    
        
        