# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main.py
#
# Description:
#            This file contains the main function used to start the software.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system

from sct_model      import SctModel     # It contains the definition of the Social Cognitive Model (SCT)
from hmpc_control   import HmpcControl  # It contains the definition of the Hybrid Model Predictive Control (HMPC)
from odeint_solver  import OdeIntSolver # It contains the loader for the ODE Integer solver from the Scypy library.
from sct_simulation import SctSimulation   # It contains the definition of the Simulation Environment.
from sys            import exit         # Importing the exit() function from the Python's isystem library.

# Main function call
if __name__ == '__main__':

    # Model Instantiation
    model   = SctModel("SCT Model")
    
    # Control Instantiation
    control = HmpcControl("HMPC")
    
    # Solver Instantiation
    # It is possible to make a comparison between different solvers.
    solver = OdeIntSolver("ODEInt")
    
    # Instantiating the sct_simulation environment 
    dmSim = SctSimulation()
    
    # Updating the model and respective control used into the embedded SCT framework  
    # and linked to the sct_simulation environment
    dmSim.Setup(model, control, solver)
    
    # Running the sct_simulation
    dmSim.Run()

    # Plotting results
    dmSim.Plot()
    
    # Finish
    exit(0)

