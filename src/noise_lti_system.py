# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  system.py
#
# Description:
#            This file defines a non-linear system class. 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from lti_system import LTISystem

#
# A class for linear time-invariant discrete time systems
# Class for linear time-invariant, discrete time systems  
# with included process and measurement noise
#
class NoiseLTISystem(LTISystem):
 
    Sw = None #np.empty()     # the covariance matrix.
    Sv = None #np.empty()     # the error covariance matrix.   
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # NoiseLTISystem(A, B, C, D, Ts, Sw, Sv, x0)
    # Constructor method
    #
    # $Inputs:
    #   A : the transition matrix. Must be square. dim = n_states x n_states
    #   B : the input matrix. Must have dim = n_states x n_inputs
    #   C : the output matrix. Must have dim = n_outputs x n_states           
    #   D : output matrix. Must have dim = n_outputs x n_inputs 
    #   Sw : the process error covariance matrix            
    #   Sv : the measurements error covariance matrix
    #   Ts : the sampling interval
    #   x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, A, B, C, D, Ts, Sw, Sv, x0 ):
        
        self.Sw = Sw
        self.Sv = Sv
        
        LTISystem.__init__(self, A, B, C, D, Ts, x0)
    
    #
    # MeasureNoise()
    # Some noise
    # $Inputs:
    #    none
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    #     
    def MeasureNoise( self ):
        return np.matrix( np.random.multivariate_normal( np.zeros((self.n_outputs,)), self.Sv, 1 ).reshape( self.n_outputs, 1) )
    
    #
    # ProcessNoise()
    # Some noise
    # $Inputs:
    #    none
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    #     
    def ProcessNoise( self ):
        return np.matrix( np.random.multivariate_normal( np.zeros((self.n_states,)), self.Sw, 1 ).reshape(self.n_states,1) )
    
    #
    # MeasureOutputs()
    # Get outputs, computed from system state
    # $Inputs:
    #    none
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    #     
    def MeasureOutputs( self ):
        return self.C * self.x + self.MeasureNoise()
    
    #
    # ApplyInput()
    # Apply single input control and update system state
    # $Inputs:
    #    u: Additional input
    # $Outputs:
    #    (Not Implemented - Overrides in derived classes
    # 
    def ApplyInput( self, u ):
        self.x = self.A * self.x + self.B * u + self.ProcessNoise()
    
                 
        
    