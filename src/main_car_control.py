# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main_car_control.py
#
# Description:
#            This file contains the main function used to start the software.
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system

import numpy as np

from pylab import *
from control import Control
from noise_lti_system import NoiseLTISystem
from kalman_state_observer import KalmanStateObserver
from simulation_environment import SimulationEnvironment



class Car( NoiseLTISystem ):
    def __init__ ( self, Ts, Sw, Sv, x0 ):
        """Our system is a car, maybe a shiny Ferrari 458 spider!
        We also encapsulate in this class the characteristics of our crappy
        GPS sensor, since the sensor is actually on the car!
        
        (Note that we have inherited from NoisyDtLTISystem.)
        
        Parameters
        ----------
        Ts : float
            the sampling time
            
        Sw : np.matrix
            the process noise covariance matrix 
            
        Sv : np.matrix
            the measurement noise covariance matrix. Our GPS is 
            quite crappy, so this may be quite high. 
            
        x0 : np.matrix, shape = (2,1)
            the initial condition of the car, i.e., position and velocity
            
        """
        
        # state space matrices, you may want to check if i 
        # did not do any mistake in the discretization!
        A = [[1, Ts], [0, 1]]
        B = [[Ts**2/2], [Ts]]
        C = [[1.0, 0.0]]
        D = [[0.0]]
        
        # we call the parent class __init__ method.
        NoiseLTISystem.__init__( self, A, B, C, D, Ts, Sw, Sv, x0 )


class Throttle( Control ):
    def __init__ ( self, value=1 ):
        """This is a nasty trick. Basically we drive on a straight by
        pushing the throttle pedal. In this case, what we are actually 
        doing is to have the full throttle! 
        
        Parameters
        ----------
        value : float
            this is basically the value of the (constant) accelleration 
            we are imposing on the car. Realistic values are 9.81 * 1 for
            a Formula 1 car and 9.81*0.2 for an old Fiat Panda.
        """
        self.value = float(value)
        
    def ComputeControlInput( self, x ):
        """Always return the same value"""
        return np.matrix([[self.value]])
    
        
if __name__ == '__main__':
    
    # sampling time. 
    Ts = 0.05
    
    # Process and measurement noise covariance matrices
    # this value should be the variance of the measurement position error.
    # Our GPS is quite crappy so i have set a Root Mean Square Error of 
    # 10 meters
    Sv = np.matrix( [[10.0**2]] )
    
    # This is a little bit less intuitive to get.
    # It accounts for the fact that even if we press the pedal to full,
    # we do not get the expected accelleration, because of the fact the 
    # our car is running in an environment with gusts, holes on the road, 
    # ..., which introduce a random variation in the car's accelleration.
    Sw = 1e1 * np.matrix( [[0.25*Ts**4, 0.5*Ts**3],[0.5*Ts**3, Ts**2]] )
    
    # Ok, now define the system, giving zero initial conditions.
    car = Car( Ts=Ts, Sw=Sw, Sv=Sv, x0 = np.matrix( [[0.0],[0.0]] ) )
    
    # define controller. We want a constant value of input. Remember to 
    # put a value for the accelleration in m/s^2, and not g!
    throttle = Throttle( value=9.81*1 ) 
    
    # create kalman state observer object
    kalman_observer = KalmanStateObserver( car )
    
    # Create simulator.
    # This is our simulation obejct; it is responsible of making all 
    # the parts communicating togheter.
    sim = SimulationEnvironment( car, throttle, kalman_observer )
        
    # run simulation for 10 seconds
    res = sim.Run( 10 )
        
    # now do plots
    
    subplot(311)
    plot ( res.t, res.y[0], 'b.', label='Position measured' )
    plot ( res.t, res.x[0], 'r-', lw=2, label='Position estimated' )
    legend(loc=2, numpoints=1)
    ylabel( 'x [m]' )
    grid()
    
    subplot(312, sharex=gca())
    plot ( res.t, res.x[1], 'r-', label='Velocity estimated' )
    ylabel( 'v [m/s]' )
    legend(loc=2, numpoints=1)
    grid()
    
    subplot(313, sharex=gca())
    plot ( res.t, res.u[0], 'b-', label='Input' )
    xlabel( 't [s]' )
    ylabel( 'u' )
    legend(loc=2, numpoints=1)
    grid()
    
    show()