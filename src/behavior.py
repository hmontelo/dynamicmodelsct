# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  behavior.py
#
# Description:
#            This file contains the implementation of the behavior class, that contains
#            the action of interest corresponding to a metric of physical activity 
#            for example.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from object import Object

# 
# Class represents a behavior into the Model
#
class Behavior(Object):
    #
    pass
   
    # Constructor method
    #def __init__(self, name):
    #   self.name = name    # instance variable unique to each instance,
                            # it represents the behavior's name.
                            
    # Set Behavior's Vale                      
    #def setValue(self, value):
    #    self.value = value  
        
    # Get the current Behavior's Name    
    #def getValue(self):
    #    return self.value
