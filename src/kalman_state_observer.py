# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  kalman_state_observer.py
#
# Description:
#            Inherits methods used to estimate the state of the system,
#             when this information is not completely available.
#             This specialization uses Kalman to make the estimative.
#
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from state_observer   import StateObserver

#
# Class that inherits and defines algorithms used to estimate the state of the system,
# when this information is not completely available.
# this class specialization use the kalman filter.
#
class KalmanStateObserver(StateObserver):
       
    xhat = None #np.empty()     # Estimate of the systems state.
    P    = None #np.empty()     # the covariance matrix.
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # KalmanStateObserver(system, x0)
    # Constructor method
    #
    # $Inputs:
    #   system : system's model
    #   x0     : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, system, x0=None ):
        # set attributes
        self.A          = system.A
        self.B          = system.B
        self.C          = system.C
        self.D          = system.D
        self.Sw         = system.Sw
        self.Sv         = system.Sv
        self.n_states   = system.n_states
        self.n_inputs   = system.n_inputs
        self.n_outputs  = system.n_outputs
        
       
        # set initial condition for state estimate
        if x0 is None:
            self.xhat = system.x + np.matrix( np.random.multivariate_normal( np.zeros((system.n_states,)), system.Sw, 1 ).reshape(system.n_states,1) )
        else:
            # check initial condition
            self.x0 = np.matrix( x0 )
            if not self.x0.shape == ( self.n_states, 1 ):
                raise ValueError("Invalid format/size of initial state vector")
            self.xhat = self.x0
        
        # Update covariance matrix of the state estimate
        self.P = self.Sw.copy()
    
    #
    # getEstimatedStates(y, u)
    # Obtain estimated states
    #
    # $Inputs:
    #   y : 
    #   u : 
    # $Outputs:
    #    none
    #   
    def getEstimatedStates( self, y, u ):
        
        y = np.asmatrix(y).reshape(self.n_outputs, 1)
        
        #simulate system with state estimate at previous step
        self.xhat = self.A * self.xhat + self.B * u
        
        # form the innovation vector
        inn = y - self.C * self.xhat
        
        # compute the covariance of the innovation
        s = self.C * self.P * self.C.T + self.Sv
        
        # form the kalman gain matrix
        K = self.A * self.P * self.C.T * np.linalg.inv(s)
        
        # update state estimate
        self.xhat += K * inn
        
        # compute covariance of the estimation error
        self.P = self.A * self.P *self.A.T -  \
                 self.A * self.P *self.C.T * np.linalg.inv(s) * \
                 self.C * self.P *self.A.T + self.Sw
        
        # return state estimate
        return self.xhat
    
    