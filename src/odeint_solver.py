# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  odeint_solver.py
#
# Description:
#            This file contains the OdeInteger class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from solver import Solver
from scipy  import integrate

#
# Class that inherits the base Solver to implement specific OdeInt solver
# attributes and methods associated to the SciPy library.
#
class OdeIntSolver(Solver):
    
    ## Constants
    DEFAULT_DELTA_T = 0.1
    
    s       = None
    delta_t = DEFAULT_DELTA_T
       
    #
    # Init(f, t_start, delta_t, initial_conditions = [])
    # Initialization/setup method
    # $Inputs:
    #    f:       isystem's function used to calculate the derivatives. 
    #    t_start: initial time
    #    delta_t: time's increment
    #    initial_conditions: Array with the initial conditions to be handled by the solver
    # $Outputs:
    #    none
    #
    def Init(self, f, t_start, delta_t, initial_conditions = []):
        # Start by specifying the integrator:
        # use ``vode`` with "backward differentiation formula"
        self.s = integrate.ode(f).set_integrator('vode', method='bdf')
        
        # Set initial condition(s): for integrating variable and time!
        self.s.set_initial_value(initial_conditions, t_start)
        
        # Set the internal time's increment.
        self.delta_t = delta_t
    
    #
    # Solve()
    # Method used to calculate the isystem's equations.
    # This method will be called during all sct_simulation time. 
    # The results are evaluated by the function "f" attributed in the initialization/setup.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def Solve(self):
        self.s.integrate(self.s.t + self.delta_t)
    
    #
    # Success()
    # Success or fail during the integration of the isystem's equation
    # $Inputs:
    #    none
    # $Outputs:
    #    Returns a boolean value indicating success or fail associated to the calculus of the
    #    isystem's equations
    #
    def Success(self):
        """Return always true if no fail is found."""
        return self.s.successful() 
     
    #
    # getResults()
    # Get all the integration results
    # $Inputs:
    #    none
    # $Outputs:
    #    Returns an array with all integration results. Useful for debug.
    # 
    def getResults(self):       
        results = []      
        for r in self.s.y:
            results.append(r)
        return results  
    
    #
    # getTime()
    # Get all the sct_simulation time  array
    # $Inputs:
    #    none
    # $Outputs:
    #    Returns an array with all times used during the integration process. Useful for debug.
    # 
    def getTime(self):
        return self.s.t
           
    #
    # PrintInit()
    # Print configured initial conditions
    # $Inputs:
    #    none
    # $Outputs:
    #    None
    #
    def PrintInit(self):
        print "Initial Conditions for OdeInt Solver:"
        print "Integrator = vode, method = bdf"

        