# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  solver.py
#
# Description:
#            This file contains implementation of the Solver class.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from object     import Object
from isolver    import ISolver

#
# Base solver class. It implements the Interface ISolver and inherits basic
# and standard methods and attributes from Object
#
class Solver(Object, ISolver):
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # Solve()
    # Method used to calculate the isystem's equations.
    # This method will be called during all sct_simulation time. 
    # The results are evaluated by the function "f" attributed in the initialization/setup.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def Solve(self):
        return
    
    #
    # Success()
    # Success or fail during the integration of the isystem's equation
    # $Inputs:
    #    none
    # $Outputs:
    #    Returns a boolean value indicating success or fail associated to the calculus of the
    #    isystem's equations
    #
    def Success(self):
        """Return always true if no fail is found."""
        return True
    
    