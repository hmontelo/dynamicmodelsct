# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  sct_simulation.py
#
# Description:
#            This file contains class definition for the Simulation Environment
#            specialized for the Social Cognitive Theory (SCT)
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
import matplotlib.pyplot as plt

 
from decision_framework import DecisionFramework
from model              import Model
from control            import Control
from solver             import Solver
from scipy.integrate    import odeint
from scipy              import integrate
from matplotlib.pylab   import *
from simulation         import Simulation

#
# Class that represents and defines a simulator into the isystem.
#   
class SctSimulation(Simulation):

    ### Simulation constants
    DEFAULT_SIM_TIME = 45.0 # seconds
    
    # Model Instantiation
    model   = Model("Model")
    
    # Control Instantiation
    control = Control("Control")
    
    # Solver Instantiation
    solver = Solver("Solver")
    
    # Framework Instantiation
    framework = DecisionFramework("Decision Framework", model, control, solver)
    
    ### Simulation parameters
    t_start     = 0.0   # initial time
    t_final     = 20    # DEFAULT_SIM_TIME
    delta_t     = 0.001 # time's increment
        
    # Number of time steps: 1 extra for initial condition
    num_steps   = np.floor((t_final - t_start)/delta_t) + 1
    
    t           = np.zeros((num_steps, 1))  # time array
    
    # Container arrays initialization.
    Skills      = np.zeros((num_steps, 1))  # Skills array - used to plot.
    OutExpected = np.zeros((num_steps, 1))  # Outcome Expected array - used to plot.
    Confidence  = np.zeros((num_steps, 1))  # Confidence array - used to plot.
    Behavior    = np.zeros((num_steps, 1))  # Behavior array - used to plot.
    BehaviorOut = np.zeros((num_steps, 1))  # Behavior Outcome array - used to plot.
    WillPower   = np.zeros((num_steps, 1))  # Will Power array - used to plot.
    
    Skill_t_zero        = 0                 # Skill at Initial time
    OutExpected_t_zero  = 0                 # Outcome Expected at Initial time
    Confidence_t_zero   = 0                 # Confidence at Initial time
    Behavior_t_zero     = 0                 # Behavior at Initial time
    OutBehavior_t_zero  = 0                 # Behavior outcome at Initial time
    WillPower_t_zero    = 0                 # Will Power at Initial time
    
    #
    # setSimulationModel(model)
    # Set the current model used by the simulator
    # $Inputs
    #    model: Model used by the simulator.
    # $Outputs:
    #    none
    #   
    def setSimulationModel(self, model):
        self.model = model
     
    #
    # getSimulationModel()
    # Get the model used by the simulator
    # $Inputs
    #    none
    # $Outputs:
    #    Returns the simulation's model
    #         
    def getSimulationModel(self):
        return self.model
        
    #
    # setSimulationControl(control)
    # Set the current control used by the simulator
    # $Inputs
    #    control: Control used by the simulator.
    # $Outputs:
    #    none
    #
    def setSimulationControl(self, control):
        self.control = control
    
    #
    # getSimulationControl()
    # Get the control used by the simulator
    # $Inputs
    #    none
    # $Outputs:
    #    Returns the simulation's control
    #     
    def getSimulationControl(self):
        return self.control

    #
    # setSimulationSolver(solver)
    # Set the current solver used by the simulator
    # $Inputs
    #    solver: Solver used by the simulator.
    # $Outputs:
    #    none
    #    
    def setSimulationSolver(self, solver):
        self.solver = solver
    
    #
    # getSimulationSolver()
    # Get the solver used by the simulator
    # $Inputs
    #    none
    # $Outputs:
    #    Returns the simulation's solver
    #    
    def getSimulationSolver(self):
        return self.solver
   
    #
    # Setup(model, control, solver)
    # Setup/Configures the simulator
    # $Inputs
    #    model: Model used by the simulator.
    #    control: Control used by the simulator
    #    solver: Solver used by the simulator
    # $Outputs:
    #    none
    #    
    def Setup(self, model, control, solver):
        self.setSimulationModel(model)
        self.setSimulationControl(control)
        self.setSimulationSolver(solver)
        self.framework.Setup(self.model, self.control, self.solver)
        
    #
    # InitialConditions()
    # Set simulator's initial conditions.
    # $Inputs
    #    none
    # $Outputs:
    #    none
    #          
    def InitialConditions(self):       
       
        self.Skill_t_zero        = 0
        self.OutExpected_t_zero  = 0
        self.Confidence_t_zero   = 0
        self.Behavior_t_zero     = 0
        self.OutBehavior_t_zero  = 0
        self.WillPower_t_zero    = 0    
        self.t[0]                = self.t_start
        
        self.model.Init(self.num_steps)
        self.control.Init()

        self.solver.Init(self.model.model, self.t_start, self.delta_t, \
                         [self.Skill_t_zero, self.OutExpected_t_zero , self.Confidence_t_zero, \
                          self.Behavior_t_zero, self.OutBehavior_t_zero, self.WillPower_t_zero \
                         ] \
                        )        
             
    
    #
    # PrintInitialConditions()
    # Print simulator's initial conditions. Useful for debug.
    # $Inputs
    #    none
    # $Outputs:
    #    none
    #    
    def PrintInitialConditions(self):
        print "1. Initial Conditions:"
        print ""        
        self.model.PrintInit()
        print ""        
        self.control.PrintInit()
       
    #
    # Run(printInternalResults = False)
    # Starts the simulation process.
    # $Inputs
    #    printInternalResults: Print internal results? (if yes
    #                          starts to print calculated values by the solver.
    #                          Useful for debug.
    # $Outputs:
    #    none
    #
    def Run(self, printInternalResults = False):
        self.PrintUTHeader()      
        self.InitialConditions()
        self.PrintInitialConditions()
                  
        # Integrate the ODE(s) across each delta_t timestep
        print "Starting Simulation...\n"
        k = 1
        while self.solver.Success() and k < self.num_steps:
       
            # Solving the differential equation
            self.solver.Solve()
     
            # Obtain results and store them to plot later
            y = self.solver.getResults()
            
            if printInternalResults == True:
                for i in y:
                    print(i),
                print "\n"
            
            self.t[k]           = self.solver.getTime()
            self.Skills[k]      = y[0]
            self.OutExpected[k] = y[1]
            self.Confidence[k]  = y[2]
            self.Behavior[k]    = y[3]
            self.BehaviorOut[k] = y[4]
            self.WillPower[k]   = y[5]
            
            self.model.ObservedBehavior[k] = self.model.xi[2] 
            self.model.ExternalCue[k] = self.model.xi[8]
            
            k += 1
            print ".",
                   
        print "\nSimulation Concluded"
    
    #
    # Plot()
    # Plot outputs and generate corresponding image (format 'png').
    # $Inputs
    #    none
    # $Outputs:
    #    none
    #    
    def Plot(self):
        # plot results
        fig = figure()
        
        ax1 = subplot(421)
        ax1.plot(self.t, self.model.ObservedBehavior)
        ax1.set_xlim(self.t_start, self.t_final)
        ax1.set_xlabel('Time [days]')
        ax1.set_ylabel('Observed Behavior, Vicarious Learning')
        ax1.grid('on')
        
        ax2 = subplot(422)
        ax2.plot(self.t, self.WillPower)
        ax2.set_xlim(self.t_start, self.t_final)
        ax2.set_xlabel('Time [days]')
        ax2.set_ylabel('Cue to Action')
        ax2.grid('on')
        
        ax3 = plt.subplot(423)
        ax3.plot(self.t, self.Confidence, 'r')
        ax3.set_xlim(self.t_start, self.t_final)
        ax3.set_xlabel('Time [days]')
        ax3.set_ylabel('Confidence')
        ax3.grid('on')
        
        ax4 = plt.subplot(424)
        ax4.plot(self.t, self.Behavior, 'r')
        ax4.set_xlim(self.t_start, self.t_final)
        ax4.set_xlabel('Time [days]')
        ax4.set_ylabel('Behavior')
        ax4.grid('on')
        
        plt.show()
     
        fig.savefig('DecisionMaking_DynamicModel_SCT.png')
        
        
    
    
    