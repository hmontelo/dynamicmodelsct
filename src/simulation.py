# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  simulation.py
#
# Description:
#            This file contains base class definition for the Simulation Environment.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules


#
# Base class for simulations.
#   
class Simulation(object):
    
    ### Simulation constants
    DEFAULT_SIM_TIME = 45.0 # seconds
    
    
    simTime = DEFAULT_SIM_TIME;
    
    #
    # PrintUTHeader()
    # Print University of Texas header. Info about Homework #1.
    # $Inputs
    #    none
    # $Outputs:
    #    none
    #     
    def PrintUTHeader(self):
        print "University of Texas at Austin"
        print "Mechanical Engineering Department"
        print "ME_396D - Decision_Control-Human_Centered_Robotics"
        print "Fall 2016, Homework #1"
        print "Hilgad Montelo - UTEID: hmd623"
        print " "
        print "--- Simulation of the Dynamic Model Based on SCT ---"
        print " "
    
    #
    # setSimulationTime(time)
    # Set the current max. time used by the simulator
    # $Inputs
    #    time: Max. Time used by the simulator.
    # $Outputs:
    #    none
    #        
    def setSimulationTime(self, time):
        self.simTime = time
    
    #
    # getSimulationTime()
    # Get the time used by the simulator
    # $Inputs
    #    none
    # $Outputs:
    #    Returns the simulation's time
    #
    def getSimulationTime(self):
        return self.simTime
    
    #
    # Run(printInternalResults = False)
    # Starts the simulation process.
    # $Inputs
    #    printInternalResults: Print internal results? (if yes
    #                          starts to print calculated values by the solver.
    #                          Useful for debug.
    # $Outputs:
    #    none
    #        
    def Run( self, Tsim, printInternalResults = False):
        pass
        
        
        