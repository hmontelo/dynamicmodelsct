# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  cue.py
#
# Description:
#            This file contains the class definition of the cues (internal and external) used to
#            model the isystem.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from object import Object

#
# Class represents the isystem's cues 
#
class Cue(Object):
    pass


#
# Class is a specialized class. Inherits from Cue class.
# Used for internal cues 
#
class InternalCue(Cue):
    pass

#
# Class is a specialized class. Inherits from Cue class. 
# Used for external cues
#
class ExternalCue(Cue):
    pass

#
# Class is a specialized class. Inherits from Cue class. 
# Used for action cues
#
class ActionCue(Cue):
    pass
