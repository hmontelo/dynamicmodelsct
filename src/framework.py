# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  framework.py
#
# Description:
#            This file contains the basis Framework class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from object     import Object
from model      import Model
from control    import Control
from solver     import Solver
from iframework import IFramework

#
# Class implements the base Framework interfaces and inherits standard 
# object functionalities
#
class Framework(Object, IFramework):
    
    # Model object used by the framework
    model   = Model("Model")
    
    # Control object used by the framework
    control = Control("Control")
    
    # Solver object used by the framework
    solver = Solver("Solver")
    
    # 
    # __init__(name, model, control, solver, i=0)
    # Constructor method used to initialize the framework.
    # $Inputs:
    #    name: the framework's name
    #    model: The model used by this framework
    #    control: The control used by this framework
    #    solver: The solver used by this framework
    #    i: selector (used to offer different configurations/initializations.
    # $Outputs:
    #    none    
    def __init__(self, name, model, control, solver, i=0):
        self.name = name
        if i != 0: 
            self.model   = model     # it represents the current model used by the framework.                              
            self.control = control   # it represents the current control used by the framework.
            self.solver  = solver    # it represents the current solver used by the framework.
    
    #
    # Setup()
    # Method used to configure the framework.
    # $Inputs:
    #    none
    # $Outputs:
    #    none
    #
    def Setup(self):
        pass

    # 
    # setModel(model)
    # Method used to set the current framework's model
    # $Inputs:
    #    model: The model used by this framework
    # $Outputs:
    #    none 
    #       
    def setModel(self, model):
        self.model = model
        
    # 
    # setControl(control)
    # Method used to set the current framework's control
    # $Inputs:
    #    control: The control used by this framework
    # $Outputs:
    #    none 
    #      
    def setControl(self, control): 
        self.control = control 
    
    # 
    # setSolver(solver)
    # Method used to set the current framework's solver
    # $Inputs:
    #    solver: The solver used by this framework
    # $Outputs:
    #    none 
    #
    def setSolver(self, solver):
        self.solver = solver
     
    # 
    # getModel()
    # Method used to get the current framework's model
    # $Inputs:
    #    none
    # $Outputs:
    #    return the current framework's model. 
    #         
    def getModel(self): 
        return self.model
    
    # 
    # getControl()
    # Method used to get the current framework's control
    # $Inputs:
    #    none
    # $Outputs:
    #    return the current framework's control. 
    # 
    def getControl(self):
        return self.control

    # 
    # getSolver()
    # Method used to get the current framework's solver
    # $Inputs:
    #    none
    # $Outputs:
    #    return the current framework's solver. 
    #
    def getSolver(self):
        return self.solver   
    