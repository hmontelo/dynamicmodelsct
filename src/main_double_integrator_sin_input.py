# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main_double_integrator_sin_input.py
#
# Description:
#            This file contains the main function used to start the software.
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system
import numpy as np
from pylab import *
from math import *
from system import System
from double_integrator import DoubleIntegrator
from control import Control


class myController( Control ):
    def __init__ ( self, t ):
        self.K = np.zeros((2,1))
        
    def ComputeControlInput( self, x ):
        #return np.asarray(math.sin(2 * math.pi * x)).reshape(1, -1)[0,:]
        #x = np.array(x)
        #u = np.sin(2 * math.pi * x);
        #return -np.dot( self.K, x)
        self.K = x      
        u = np.sin(2*pi*10*self.K)
        return u
    

if __name__ == '__main__':
    
    # this is the sampling time
    Ts = 0.1
    
    # initial condition
    x0 = np.matrix( [[0.0],[0.0]] )
    
    # create a Double integrator object
    #di = SingleIntegrator( Ts, w0=1e-4, x0=np.matrix([[5]]))
    di = DoubleIntegrator( Ts, x0 = x0 )
    
    # define controller
    #controller = LQControl( di, Q=1*np.eye(2), R=0.1*np.eye(1) )
    
    # set input sequence vector
    t = np.linspace(0,1,1000)
    u = np.sin(2*pi*10*t)
    
    # simulate system response
    y = di.Simulate(u)
       
   
 
    # plot system response
    
    subplot(211)
    plot(t, y[0])
    xlabel('t [s]')
    ylabel('x [m]')
    grid()
    
    subplot(212)
    plot(t, y[1])
    xlabel('t [s]')
    ylabel('v [m/s]')
    grid()
    
    show()
