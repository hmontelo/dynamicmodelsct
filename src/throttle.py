# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  simulation_results.py
#
# Description:
#            This file contains class definition for the Simulation Results
#            A specialized class used to hold and show the simulation results
#            in a previous simulation execution.
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
import random

from control import Control


#
# The class represents a base vehicle class.
# The class inherits from NoiseLTISystem, which makes the vehicle
# consider noise during the simulations.
#   
class Throttle( Control ):
    
    value = 0;  # A factor to improve the vehicle's accelleration 
                #        we are imposing on the car. Ex.: 0.2 (1 * 9.81) for a very fast vehicle
                #        and 0.2 (0.2 * 9.81), for instance, for a slow one
    
    #
    # Throttle(value)
    # Constructor method
    #
    # $Inputs:
    # value : a factor to improve the vehicle's acceleration 
    #        we are imposing on the car. Ex.: 1 for a very fast vehicle
    #        and 0.2, for instance, for a slow one.
    # $Outputs:
    #    none
    #
    def __init__ ( self, value=1 ):
        
        self.value = float(value) 
        
    
    #
    # ComputeControlInput( x  )
    # Compute static state feedback
    #
    # $Inputs:
    #    x : It is the the state column vector  
    #
    # $Outputs:
    #    It returns an input vector which is feedback back to the system
    #
    def ComputeControlInput( self, x ):
        #return np.matrix([[random.random() * self.value]])
        return np.matrix([[self.value]])
    
    
    