# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  decision_framework.py
#
# Description:
#            This file contains the Decision-Making Framework class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#

# Importing specific modules
from framework import Framework

#
# Class used to setup and mount the decision making scenario.
# The class inherits the base framework class and override
# the setup method.
#
class DecisionFramework(Framework):
    
    #
    # Setup(model, control, solver)
    # Method used to configure the framework.
    # $Inputs:
    #    model: The model used by this framework
    #    control: The control used by this framework
    #    solver: The solver used by this framework
    # $Outputs:
    #    none
    #
    def Setup(self, model, control, solver):
        self.setModel(model)
        self.setControl(control)
        self.setSolver(solver)
        
    