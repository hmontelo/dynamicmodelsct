# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  main_double_integrator.py
#
# Description:
#            This file contains the main function used to start the software.
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules used to simulate the dynamic model of the system

import numpy as np
from pylab import *

from double_integrator import DoubleIntegrator
from lq_control import LQControl
from simulation_environment import SimulationEnvironment

              
if __name__ == '__main__':
    
    # this is the sampling time
    Ts = 1.0 / 40
    
    # initial condition
    x0 = np.matrix( [[-6.0],[0.0]] )
    
    # define system
    di = DoubleIntegrator( Ts=Ts, x0=x0 )
    
    # define controller
    controller = LQControl( di, Q=1*np.eye(2), R=0.1*np.eye(1) ) 
    
    # create simulator
    sim = SimulationEnvironment( di, controller )
    
    # run simulation
    res = sim.Run( 6 )
        
    # plot results
    subplot(311)
    plot ( res.t, res.x[0], '-' )
    ylabel('x [m]')
    grid()
    
    subplot(312, sharex=gca())
    plot ( res.t, res.x[1] )
    ylabel('v [m/s]')
    grid()
    
    subplot(313, sharex=gca())
    plot ( res.t, res.u[0] )
    xlabel('t [s]')
    ylabel('u')
    grid()
    
    show()