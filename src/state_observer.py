# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  state_observer.py
#
# Description:
#            Defines common methods used to estimate the state of the system,
#             when this information is not completely available.
#
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from observer   import Observer

#
# Class that implements algorithms used to estimate the state of the system,
# when this information is not completely available
#
class StateObserver(Observer):
    
    A  = None #np.empty()     # the transition matrix.
    B  = None #np.empty()     # the input matrix
    C  = None #np.empty()     # the output matrix
    D  = None #np.empty()     # output matrix
    Sw = None #np.empty()     # the covariance matrix.
    Sv = None #np.empty()     # the error covariance matrix.
    n_states = 0        # number of states
    n_inputs = 0        # number of inputs
    n_outputs= 0        # number of outputs
    x0 = None #np.empty()     # initial system state array
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # getEstimatedStates(y, u)
    # Obtain estimated states
    #
    # $Inputs:
    #   y : 
    #   u : 
    # $Outputs:
    #    none
    #   
    def getEstimatedStates( self, y, u ):
        pass
    
    