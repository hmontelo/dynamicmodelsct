# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  double_integrator.py
#
# Description:
#            This file defines a non-linear system class. 
#            system class
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np
from noise_lti_system import NoiseLTISystem

#
# A class for linear time-invariant discrete time systems
#
class SingleIntegrator(NoiseLTISystem):
       
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # SingleIntegrator(Ts, w0, x0)
    # Constructor method
    #
    # $Inputs:
    #   Ts : the sampling interval
    #   w0 : the variance of the process noise
    #   x0 : the initial system state
    # $Outputs:
    #    none
    #
    def __init__ ( self, Ts, w0, x0 ):       
        # state space matrices
        # We can create lists of lists,
        # then internally they will be converted
        # to numpy matrices
        A = [[1]]
        B = [[Ts]]
        C = [[1]]
        D = [[0]]
        
        # process noise and measurement noise. Measurement noise
        # is set to be very low.
        Sw = np.matrix( [[w0]] )
        Sv = np.matrix( [[1e-9]] )
        
        NoiseLTISystem.__init__( self, A, B, C, D, Ts, Sw, Sv, x0 )
    