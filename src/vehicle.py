# Decision Making Framework based on Social Cognition theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  simulation_results.py
#
# Description:
#            This file contains class definition for the Simulation Results
#            A specialized class used to hold and show the simulation results
#            in a previous simulation execution.
# Updates:
#            Date/Time        Author                 Description
#            Sep 30, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np

from noise_lti_system import NoiseLTISystem


#
# The class represents a base vehicle class.
# The class inherits from NoiseLTISystem, which makes the vehicle
# consider noise during the simulations.
#   
class Vehicle( NoiseLTISystem ):
    
    #
    # Vehicle(Ts, Sw, Sv, x0)
    # Constructor method
    #
    # $Inputs:
    #   Ts : the sampling time            
    #   Sw : the process noise covariance matrix 
    #   Sv : the measurement noise covariance matrix.
    #        It is considered for this model a low accuracy GPS. 
    #   x0 : the initial condition of the vehicle (position and velocity)
    # $Outputs:
    #    none
    #
    def __init__ ( self, Ts, Sw, Sv, x0 ):       
        # state space matrices, you may want to check if i 
        # did not do any mistake in the discretization!
        A = [[1, Ts], [0, 1]]
        B = [[Ts**2/2], [Ts]]
        C = [[1.0, 0.0]]
        D = [[0.0]]
        
        # we call the parent class constructor method
        NoiseLTISystem.__init__(self, A, B, C, D, Ts, Sw, Sv, x0 ) 
    
    
    