# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  control.py
#
# Description:
#            This file contains the a non Linear control class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 31, 2016     Hilgad.Montelo         File was created
#

# Importing specific modules
import numpy as np

from control   import Control

#
# Class implements a non Linear control class definition
#
class NonLinearController( Control ):
    
    # Overriding the Initialize method to specific values used for the
    # control class                     
    def Init(self):
        pass
    
    #
    # NonLinearController(self, K, alpha, saturation )
    # Constructor method
    #
    # $Inputs:
    #    K : constant.       
    #    alpha : constant   
    #    saturation : saturation matrix
    # $Outputs:
    #    none
    #
    def __init__ ( self, K, alpha, saturation ):
        self.K = K
        self.alpha = alpha
        self.saturation = saturation
    
    #
    # ComputeControlInput( x  )
    # Control input is a static state feedback
    # The control input can saturate
    #    of the form: 
    #        u = K * |x|^alpha * sign(x)
    #    where x is the state and K and alpha are 
    #    to constants
    #
    # $Inputs:
    #    x : It is the the state column vector  
    # $Outputs:
    #    It returns an input vector which is feedback back to the isystem
    #   
    def ComputeControlInput( self, x ):
        u = -self.K * np.abs(x)**self.alpha * np.sign(x)
        return np.clip(u, self.saturation[0], self.saturation[1])
    