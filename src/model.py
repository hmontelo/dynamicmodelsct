# Decision Making Framework based on Social Cognition Theory (SCT)
# Copyright (C) 2016 Hilgad Montelo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Filename:  model.py
#
# Description:
#            This file contains the abstract model class definition.
# Updates:
#            Date/Time        Author                 Description
#            Sep 6, 2016      Hilgad.Montelo         File was created
#


# Importing specific modules
from imodel import IModel
from object import Object

#
# Base model class that implements the interface IModel and 
# the inherits the standards attributes and methods from the object class.
# The class defines a model into the system. 
#
class Model(Object, IModel):
    
    # 
    # Evaluate()
    # General method to evaluate the model.   
    # $Inputs:
    #    none
    # $Outputs:
    #    none    
    # 
    def Evaluate(self):
        return
    
    # 
    # Init(steps)
    # Initialize the model
    # Overriding the Initialize method to specific values used for the
    # model class      
    # $Inputs:
    #    steps: steps used by the sct_simulation environment
    # $Outputs:
    #    none                   
    def Init(self, steps):
        pass


    
    
    